#include <iostream>
#include <winbgim.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <thread>
#include <chrono>
#include<bits/stdc++.h>
#include <math.h>
#include <limits.h>
#include <ctype.h>
#define infinit INT_MAX
#define epsi 0.0001
#define  MAX 100
#define MAX1 20

using namespace std;
struct nod
{
    char value;
    nod* left, *right;
};

typedef nod * arbore;

int heightTree (arbore a)
{
    if (a == NULL)
        return 0;
    else
    {
        int h1 = heightTree(a->left);
        int h2 = heightTree(a->right);
        return 1 + max(h1, h2);
    }
}

bool esteArboreNul(arbore a)
{
    return (a == NULL);
}
void initArbore(arbore& a)
{
    if (!esteArboreNul(a)) a = NULL;
}

bool adaugaLaArboreElement(arbore &a, char el)
{
    if (esteArboreNul(a))
    {
        a = new nod;
        if (!a) return false;
        a->value = el;
        a->left = NULL;
        a->right = NULL;
        return true;
    }
    else if (el < a->value)
        return adaugaLaArboreElement(a->left, el);
    else
        return adaugaLaArboreElement(a->right, el);
}

bool existaElementInArbore(arbore a,int el)
{
    if (!esteArboreNul(a))
    {
        if (a->value == el) return true;
        else if (el < a->value)
            return existaElementInArbore(a->left, el);
        else
            return existaElementInArbore(a->right, el);
    }
    else
        return false;
}
void creare_arbore(arbore &a, int n)
{
    int x;
    for (int i = 1; i <= n; i++)
    {
        cout << ("introduceti elementul ") << i << (" al arborelui : ");
        cin >> x;
        adaugaLaArboreElement(a,x);
    }
}

int widthTree(arbore a)
{
    if (a == NULL)
        return 0;
    else
    {
        int w1 = widthTree(a->left);
        int w2 = widthTree(a->right);
        return 1 + w1 + w2;
    }
}

void append(char* s, char c)
{
    int len = strlen(s);
    s[len] = c;
    s[len+1] = '\0';
}

void deseneazaNod(char elem, int xc, int yc, int latime, int inaltime)
{
    char arr[5]="";
    append(arr,elem);
    setcolor(WHITE);
    setfillstyle(SOLID_FILL,BLUE);
    fillellipse(xc,yc,textwidth(arr)+2, textheight("M")/2+6);
    settextstyle(SANS_SERIF_FONT,HORIZ_DIR,1);
    setbkcolor(BLUE);
    setcolor(GREEN);
    settextjustify(1,1);
    outtextxy(xc,yc+4,arr);
}

void deseneazaArbore(nod *a, int niv, int stanga, int latime, int inaltime)
{
    if(a!=NULL)
    {
        int n1=widthTree(a->left);
        int xc=stanga+latime*n1+latime/2;
        int yc=niv*inaltime-inaltime/2;

        if (a->left!=NULL)
        {
            int xcs=stanga+latime*widthTree(a->left->left)+latime/2;
            setcolor(WHITE);
            line(xc,yc,xcs,yc+inaltime);
        }
        if (a->right!=NULL)
        {
            int xcd=stanga+latime*(n1+1)+latime*widthTree(a->right->left)+latime/2;
            setcolor(WHITE);
            line(xc,yc,xcd,yc+inaltime);
        }
        deseneazaNod(a->value,xc,yc,latime, inaltime);
        deseneazaArbore(a->left,niv+1,stanga, latime, inaltime);
        deseneazaArbore(a->right,niv+1,stanga+latime*(n1+1), latime, inaltime);
    }
}

int prec(char c)
{
    if(c == '^')
        return 3;
    else if(c == '/' || c=='*')
        return 2;
    else if(c == '+' || c == '-')
        return 1;
    else
        return -1;
}


string infixToPostfix(string s)
{

    stack<char> st; //For stack operations, we are using C++ built in stack
    string result;

    for(int i = 0; i < s.length(); i++)
    {
        char c = s[i];

        // If the scanned character is
        // an operand, add it to output string.
        if((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9'))
            result += c;

        // If the scanned character is an
        // (, push it to the stack.
        else if(c == '(')
            st.push('(');

        // If the scanned character is an ),
        // pop and to output string from the stack
        // until an ( is encountered.
        else if(c == ')')
        {
            while(st.top() != '(')
            {
                result += st.top();
                st.pop();
            }
            st.pop();
        }

        //If an operator is scanned
        else
        {
            while(!st.empty() && prec(s[i]) <= prec(st.top()))
            {
                result += st.top();
                st.pop();
            }
            st.push(c);
        }
    }

    // Pop all the remaining elements from the stack
    while(!st.empty())
    {
        result += st.top();
        st.pop();
    }

    cout << result << endl;
    return result;
}



// A utility function to check if 'c'
// is an operator
bool isOperator(char c)
{
    if (c == '+' || c == '-' ||
            c == '*' || c == '/' ||
            c == '^')
        return true;
    return false;
}

// Utility function to do inorder traversal
void inorder(nod *t)
{
    if(t)
    {
        inorder(t->left);
        printf("%c ", t->value);
        inorder(t->right);
    }
}

// A utility function to create a new node
nod* newNode(char v)
{
    nod *temp = new nod;
    temp->left = temp->right = NULL;
    temp->value = v;
    return temp;
};

// Returns root of constructed tree for given
// postfix expression
nod* constructTree(string postfix)
{
    stack<nod *> st;
    nod *t, *t1, *t2;

    // Traverse through every character of
    // input expression
    for (int i=0; i< postfix.length(); i++)
    {
        // If operand, simply push into stack
        if (!isOperator(postfix[i]))
        {
            t = newNode(postfix[i]);
            st.push(t);
        }
        else // operator
        {
            t = newNode(postfix[i]);

            // Pop two top nodes
            t1 = st.top(); // Store top
            st.pop();      // Remove top
            t2 = st.top();
            st.pop();

            //  make them children
            t->right = t1;
            t->left = t2;

            // Add this subexpression to stack
            st.push(t);
        }
    }

    //  only element will be root of expression
    // tree
    t = st.top();
    st.pop();

    return t;
}


int nr_corect_de_paranteze(char a[])
{
    int i,s1=0,s2=0;
    for (i=0; i<strlen(a); i++)
    {
        if (a[i]=='(')
            s1++;

        if (a[i]==')')
            s2++;
    }
    if (s1==s2)
        return s1;//numarul de paranteze deschise
    else
        return 99;
}

void simplificare(char a[])
{
    int i,n;
    for (i=0; i<strlen(a); i++)
    {
        if (isdigit(a[i]) && isdigit(a[i+1]))
        {
            strcpy(a+i,a+i+1);
        }
        if(a[i]=='l' && a[i+1]=='n')
        {
            strcpy(a+i,a+1+2);
        }
        if(a[i]=='s' && a[i+1]=='i' && a[i+2]=='n')
        {
            strcpy(a+i,a+i+3);
        }
        if(a[i]=='c' && a[i+1]=='o' && a[i+2]=='s')
        {
            strcpy(a+i,a+i+3);
        }
        if(a[i]=='(' && a[i+2]==')' && isdigit(a[i+1]))
        {
            int j=a[i+1];
            strcpy(a+i,a+i+2);
            a[i]=j;
        }

    }
}

int necunoscute(char c[])
{
    int i,a,x,y;
    for (i=0,x=0,y=0; i<strlen(c); i++)
    {
        a=c[i];
        if (a==120) x++;
        if (a==121) y++;
    }
    if((x!=0) && (y!=0)) return 2;
    if((x!=0) && (y==0)) return 1;
    if((x==0) && (y!=0)) return 3;
    else
        return 0;
}

int corectitudine(char a[])
{
    char operatori[200]="+-*/^";
    int t=1;
    int i;
    if(nr_corect_de_paranteze(a)==99)
    {
        t=t*0;
    }
    else if(strchr(operatori,a[0]) || a[0]==')' || a[0]=='.')
    {
        t=t*0;
    }
    else if(strchr(operatori,a[strlen(a)-1]) || a[strlen(a)-1]=='(' || a[strlen(a)-1]=='.')
    {
        t=t*0;
    }
    else
    {
        simplificare(a);
        for(i=0; i<strlen(a); i++)
        {

            if(isdigit(a[i]))
            {
                if(isalpha(a[i+1]) || a[i]=='(')
                {
                    t=t*0;
                }
                else
                {
                    t=t*1;
                }
            }
            else if(isalpha(a[i]) && (a[i]!='x' || a[i]!='y'))
            {
                t=t*0;
            }
            else if(strchr(operatori,a[i]))
            {
                if(strchr(operatori,a[i+1]) || (a[i+1]==')'))
                {
                    t=t*0;
                }
                else
                {
                    t=t*1;
                }
            }
            else if(a[i]=='(')
            {
                if(strchr(operatori,a[i+1]) || (a[i+1]==')') || (isalpha(a[i+1]) && (a[i+1]!='x' || a[i+1]!='y')))
                {
                    t=t*0;
                }
                else if(isdigit(a[i+1]) || a[i+1]=='x' || a[i+1]=='y')
                {
                    t=t*1;
                }
            }
            else if(a[i]==')')
            {
                if(strchr(operatori,a[i-1]) || (isalpha(a[i+1]) && (a[i+1]!='x' || a[i+1]!='y')))
                {
                    t=t*0;
                }
                else if(isdigit(a[i+1]) || a[i+1]=='x' || a[i+1]=='y')
                {
                    t=t*1;
                }
            }
            else if(a[i]=='/')
            {
                if((a[i+1]=='0') )
                {
                    t=t*0;
                }
            }
            else
            {
                t=t*1;
            }
        }
    }
    return t;
}


int afisare( char a[], char c[])
{

    if(corectitudine(a)==0)
    {
        cout<<"Functia este incorecta";
        return 0;
    }
    else
    {
        cout<<"Functia nu este incorecta";
        return 1;
    }
}

void citire_functie()
{
    settextstyle(8,0,3);
    outtextxy(50,50,"Introduceti functia:");
    readimagefile("delete.jpg",700,50,750,100);
    readimagefile("enter.jpg",700,150,750,200);
    char a[100],c[100];
    int i,j;
    int buton,x,y;
    bool click=false;

    for(i=0; 1<100; i++)
    {
        a[i]=getch();
        if(a[i]!='.')
        {
            outtextxy(300,100,a);
            while(!click)
            {
                if(ismouseclick(WM_LBUTTONDOWN))
                {
                    clearmouseclick(WM_LBUTTONDOWN);
                    x=mousex();
                    y=mousey();
                    if((x>=700 && x<=750) && (y>=50 && y<=100))
                    {
                        click=true;
                        a[i]=' ';
                        i--;
                    }
                }
            }
            while(!click)
            {
                if(ismouseclick(WM_LBUTTONDOWN))
                {
                    clearmouseclick(WM_LBUTTONDOWN);
                    x=mousex();
                    y=mousey();
                    if((x>=700 && x<=750) && (y>=150 && y<=200))
                    {
                        click=true;
                        a[i]='.';
                    }
                }
            }
        }
        else outtextxy(100,300,"Functia a fost introdusa cu succes");
    }
}

void informatii_generale();
void inceput()
{
    int buton,x,y;
    settextstyle(8,0,4);
    outtextxy(250,350,"Are you ready?");
    settextstyle(10,0,2);
    outtextxy(330,400,"S t a r t");
    readimagefile("undo.jpg",50,500,100,550);
    bool click=false;
    while(!click)
    {
        if(ismouseclick(WM_LBUTTONDOWN))
        {
            clearmouseclick(WM_LBUTTONDOWN);
            x=mousex();
            y=mousey();
            if((x>=330 && x<=400) && (y>=370 && y<=420))
            {
                click=true;
                cleardevice();
                citire_functie();
            }
        }

        if(ismouseclick(WM_LBUTTONDOWN))
        {
            clearmouseclick(WM_LBUTTONDOWN);
            x=mousex();
            y=mousey();
            if((x>=50 && x<=100) && (y>=500 && y<=550))
            {
                click=true;
                cleardevice();
                informatii_generale();
            }
        }
    }
}

void informatii_generale()
{
    int buton,x,y;
    settextstyle(6,0,1);
    readimagefile("next.jpg",700,500,750,550);
    outtextxy(50,400,"Proiect IP nr.23 ");
    outtextxy(50,420,"Math - Editor de formule matematice");
    outtextxy(50,440,"Robu Victor / Sîrbu Valentina / 1A1");
    bool click=false;
    while(!click)
    {
        if(ismouseclick(WM_LBUTTONDOWN))
        {
            clearmouseclick(WM_LBUTTONDOWN);
            x=mousex();
            y=mousey();
            if((x>=700 && x<=750) && (y>=500 && y<=550))
                click=true;
        }
    }
    cleardevice();
    inceput();
}

int post_pp(char c[],int n, int i)
{
    for(int j=i; j<strlen(c); j++)
    {
        if(c[j]=='(')
        {
            n=n+1;
            post_pp(c,n,j+1);
        }
        if(c[j]==')')
        {
            n=n-1;
            if(n!=0)
            {
                post_pp(c,n,j+1);
            }
            if(n==0)
            {
                return j ;
            }
        }
    }
}

int before_pp(char c[],int n, int i)
{
    for(int j=i; j>=0; j--)
    {
        if(c[j]==')')
        {
            n=n+1;
            before_pp(c,n,j-1);
        }
        if(c[j]=='(')
        {
            n=n-1;
            if(n!=0)
            {
                before_pp(c,n,j-1);
            }
            if(n==0)
            {
                return j;
            }
        }
    }
}

int dimensiune_formula(char c[],char dim[])
{
    char Op[200]="+-/*^()";
    int lungime,inaltime=1,l1=0,l2=0,a1=0,a2=0;
    lungime=strlen(c);
    cout<<lungime;
    string r;

    for(int i=0,n=0; i<strlen(c); i++)
    {
        if(c[i]=='/')
        {
            lungime=lungime-1;
            inaltime=inaltime+1;
            if(c[i-1]==')')
            {
                a1=i-before_pp(c,n,i);
                l1=2;
            }
            if(c[i-1]!=')')
            {
                int j=i-1;
                while(!strchr(Op,c[j]))
                {
                    a1++;
                    j--;
                }
            }
            if(c[i+1]=='(')
            {
                a2=post_pp(c,n,i)-i;
                l2=2;
                i=post_pp(c,n,i)+1;
            }
            if(c[i+1]!='(')
            {
                int j=i+1;
                while(!strchr(Op,c[j]))
                {
                    a2++;
                    j++;
                }
                i=j;
            }
        }
        if(a1>=a2)
        {
            if(l1 || l2)
            {
                lungime=lungime-a2-2;
            }
            else
            {
                lungime=lungime-a2;
            }
        }
        else
        {
            if(l1 || l2)
            {
                lungime=lungime-a1-2;
            }
            else
            {
                lungime=lungime-a1;
            }
        }
        //+ pentru puteri si radicali
    }

    dim[1]=lungime;
    cout<<dim[1];
    dim[2]=inaltime;
    cout<<dim[2];
    return lungime;

}


char Opp[200]="+-/*^";
stack<char> operatii;
stack<char> numar;
const float pi=3.1415926;
char top1=operatii.top(),top2=numar.top();

void citire_in_stiva(char c[])
{
    for(int i=0,n=0; i<=strlen(c); i++)
    {
        if((c[i] >= 'a' && c[i] <= 'z') || (c[i] >= 'A' && c[i] <= 'Z') || (c[i] >= '0' && c[i] <= '9'))
            numar.push(c[i]);
        if(strchr(Opp,c[i]));
        operatii.push(c[i]);
    }
}

int Prioritate(char c)  // prioritate operatorilor
{
    if(c=='(' || c==')')
        return 0;
    if(c=='+' || c=='-')
        return 1;
    if(c=='*' || c=='/')
        return 2;
    if(c=='^')
        return 3;
    if(c=='=' || c=='#' || c=='<' || c=='>')
        return 4;
    if(c=='c' || c=='s' || c=='l' || c=='e' || c=='t' || c=='a' || c=='r')
        return 5;
}

bool DifInf(float x)
{
    return fabs(infinit-fabs(x)) > infinit / 2.0;
}

bool EsteNumar(char sir[MAX1])
{
    return (atof(sir)!=0.0 &&
            strchr("0123456789",sir[0]));
}

float Inmultit(float x, float y)
{
    if (fabs(x < epsi) || fabs(y) < epsi) return 0;
    else if (DifInf(x) && DifInf(y)) return x*y;
    else return infinit;
}

float Plus(float x, float y)
{
    if (DifInf(x) && DifInf(y))  return x+y;
    else return infinit;
}

float Minus(float x, float y)
{
    if (DifInf(x) && DifInf(y))  return x-y;
    else return infinit;
}

float Impartit(float x, float y)
{
    if (fabs(y)>epsi) return x/y;
    else return infinit;
}

float Sinus(float x)
{
    if (DifInf(x))  return sin(x);
    else return infinit;
}

float Cosinus(float x)
{
    if (DifInf(x))  return cos(x);
    else return infinit;
}









int main()
{
    char a[100],c[100],dim[50];
    cout << ("introduceti sir= ");
    cin.getline(a,100);
    strcpy(c,a);
    //afisare(a,c);
 //   dimensiune_formula(c,dim);



    /* int height=600, width=800;
     initwindow(width,height);
     citire_in_stiva(c);

    */






     if(afisare(a,c)==1)
     {
         nod* radacina;
         string postfix = infixToPostfix(c);
         radacina = constructTree(postfix);
         cout << postfix;
         informatii_generale();

         int height=600, width=800;
         initwindow(width,height);
         setcolor(WHITE);
         setbkcolor(BLUE);
         cleardevice();
         rectangle(1,1,width-1,height-1);
         int latime, inaltime;
         latime=width/widthTree(radacina);
         inaltime=height/heightTree(radacina);
         deseneazaArbore(radacina,1,0,latime,inaltime);

         getch();
         closegraph();
     }













    return 0;
}

#include <iostream>
#include <math.h>
#include <graphics.h>
#include <winbgim.h>
#include <sstream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

using namespace std;

struct nod {
    char info;
    nod * stang;
    nod * drept;
    nod * parinte;
    };

typedef nod * arbore;

int heightTree (arbore a) {
    if (a == NULL)
        return 0;
    else {
        int h1 = heightTree(a->stang);
        int h2 = heightTree(a->drept);
        return 1 + max(h1 , h2);
    }
}

bool esteArboreNul(arbore a)
{
    return (a == NULL);
}
void initArbore(arbore& a)
{
    if (!esteArboreNul(a)) a = NULL;
}

bool adaugaLaArboreElement(arbore &a, int el, arbore parinte) {
    if (esteArboreNul(a)) {
        a = new nod;
        if (!a) return false;
        a->info = el;
        a->stang = NULL;
        a->drept = NULL;
        a->parinte=parinte;
        return true;
		}
		else
			if (el < a->info)
        		return adaugaLaArboreElement(a->stang, el,a);
			else
				return adaugaLaArboreElement(a->drept, el,a);
}

bool existaElementInArbore(arbore a,int el)
{
    if (!esteArboreNul(a))
    {
        if (a->info == el) return true;
        else
            if (el < a->info)
                return existaElementInArbore(a->stang, el);
            else
                return existaElementInArbore(a->drept, el);
    }
    else
        return false;
}

void parcurgereInPreordine(arbore a)
{
    if (!esteArboreNul(a))
    {
        cout << a->info << ", ";
        parcurgereInPreordine(a->stang);
        parcurgereInPreordine(a->drept);
    }
}

void parcurgereInPostordine(arbore a)
{
    if (!esteArboreNul(a))
    {
        parcurgereInPostordine(a->stang);
        parcurgereInPostordine(a->drept);
        cout << a->info << ", ";
    }
}

void parcurgereInInordine(arbore a)
{
    if (!esteArboreNul(a))
    {
        parcurgereInInordine(a->stang);
        cout << a->info <<", ";
        if (a->parinte != NULL) {
            cout << ("{") << a->parinte->info << ("} ");
        }
        parcurgereInInordine(a->drept);
    }
}

void creare_arbore(arbore &a, int n) {
    int x;
    for (int i = 1; i <= n; i++) {
        cout << ("introduceti elementul ") << i << (" al arborelui : ");
        cin >> x;
        adaugaLaArboreElement(a,x,NULL);
    }
}

int widthTree(arbore a) {
    if (a == NULL)
        return 0;
    else {
        int w1 = widthTree(a->stang);
        int w2 = widthTree(a->drept);
        return 1 + w1 + w2;
    }
}

void outputContent(int info, int nivel, int coloana, int hNivel, int lNivel) {
    int posCenterX = (coloana - 1) * lNivel + lNivel / 2;
    int posCenterY = (nivel - 1) * hNivel + hNivel / 2;
    char sir[33];
    sprintf(sir, "%d", info);
    setcolor(RED);
    ellipse(posCenterX, posCenterY, 0, 360, lNivel / 2, hNivel / 2);
    setfillstyle(SOLID_FILL, WHITE);
    fillellipse(posCenterX, posCenterY, lNivel / 2, hNivel / 2);
    outtextxy(posCenterX - textwidth(sir) / 2, posCenterY - textheight(sir) / 2, sir);
}

void drawTree(arbore a, int nivel, int stang, int hNivel, int lNivel, int width) {
int coloana;
    if (a != NULL) {
            if (stang ==1) {
                 coloana = 1 + widthTree(a->stang);}
            else {
                 coloana =  (width - (width - widthTree(a->parinte)));
            }
        outputContent(a->info, nivel, coloana, hNivel, lNivel);
        drawTree(a->stang, nivel + 1, 1, hNivel, lNivel, width);
        drawTree(a->drept, nivel + 1, 0, hNivel, lNivel, width);
    }
}


int main()
{   arbore a;
    int width = 800;
    int height = 600;
    int n;

    cout << ("introduceti numarul de noduri al arborelui : ");
    cin >> n;

    initArbore(a);
    creare_arbore(a,n);

    int heightLevel = height / heightTree(a);
    int widthLevel = width / widthTree(a);

    parcurgereInInordine(a);
    //cout << heightTree(a) << endl;

    initwindow(width, height);
    drawTree(a, 1, 1, heightLevel, widthLevel, widthTree(a));
    getch();
    closegraph();

    return 0;
}

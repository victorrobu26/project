#include <iostream>
#include <winbgim.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <thread>

using namespace std;

string post_pp(string c,int n, int i,string r)
{
    for(int j=i; j < c.length(); j++)
    {
        if(c[j]!='(' && c[j]!=')')
        {
            r=r+c[j];
        }
        if(c[j]=='(')
        {
            if(n)
            {
                r=r+c[j];
            }
            n=n+1;
            post_pp(c,n,j+1,r);
        }
        if(c[j]==')')
        {
            n=n-1;
            if(n!=0)
            {
                r=r+c[j];
                post_pp(c,n,j+1,r);
            }
            if(n==0)
            {
                return r ;
            }
        }
    }
}

string before_pp(string c,int n, int i,string r)
{
    for(int j=i; j>=0; j--)
    {
        if(c[j]!='(' && c[j]!=')')
        {
            r=c[j]+r;
        }
        if(c[j]==')')
        {
            if(n)
            {
                r=c[j]+r;
            }
            n=n+1;
            before_pp(c,n,j-1,r);
        }
        if(c[j]=='(')
        {
            n=n-1;
            if(n!=0)
            {
                r=c[j]+r;
                before_pp(c,n,j-1,r);
            }
            if(n==0)
            {
                return r;
            }
        }
    }
}

char Op[200]="+-/*^()";

int m_formula(string c,int i,int m)
{
    while(i >= 0)
    {
        int n=0,a1=0,a2=0,l1=0,l2=0;
        string r,v;
        if(c[i]=='/' || c[i]=='^')
        {
            if(!m)
            {
                m++;
            }
            if(c[i-1]==')')
            {
                v=before_pp(c,n,i-1,r);
                i=i-v.length()-2;
                for(int k=0; k<v.length(); k++)
                {
                    if(v[k]=='/' || v[k]=='^')
                    {
                        m++;
                        l1=v.length();
                        a1=m_formula(v,l1,m);
                        break;
                    }
                }
            }
            if(c[i+1]=='(')
            {
                v=post_pp(c,n,i+1,r);
                for(int k=0; k<v.length(); k++)
                {
                    if(v[k]=='/' || v[k]=='^')
                    {
                        m++;
                        l2=v.length();
                        a2=m_formula(v,l2,m);
                        break;
                    }
                }
            }
        }
        i--;
    }
    return m;

}
int dim_formula(string c,int i,int lungime)
{
    while(i >= 0)
    {
        int n=0,l1=0,l2=0,a1=0,a2=0,g=i-1,h=i+1;
        string r,v;

        if(c[i]=='/')
        {
            lungime=lungime-1;
            if(c[i-1]==')')
            {
                lungime=lungime-2;
                v=before_pp(c,n,i-1,r);
                i=i-v.length()-2;
                for(int k=0; k<v.length(); k++)
                {
                    if(v[k]=='/')
                    {
                        l1=v.length();
                        a1=dim_formula(v,l1,l1);
                        break;
                    }
                    else
                    {
                        a1=v.length();
                    }
                }
            }
            if(c[i-1]!=')')
            {
                while(!strchr(Op,c[g]) && g>=0)
                {
                    a1++;
                    g--;
                }
            }
            if(c[i+1]=='(')
            {
                lungime=lungime-2;
                v=post_pp(c,n,i+1,r);
                for(int k=0; k<v.length(); k++)
                {
                    if(v[k]=='/')
                    {
                        l2=v.length();
                        a2=dim_formula(v,l2,l2);
                        break;
                    }
                    else
                    {
                        a2=v.length();
                    }
                }
            }
            if(c[i+1]!='(')
            {
                while(!strchr(Op,c[h]) && h< c.length())
                {
                    a2++;
                    h++;
                }
            }
            if(a1>=a2)
            {
                if(l2)
                {
                    lungime=lungime-l2;
                }
                else
                {
                    lungime=lungime-a2;
                }

            }
            else
            {
                if(l1)
                {
                    lungime=lungime-l1;
                }
                else
                {
                    lungime=lungime-a1;
                }
            }
        }
        if(c[i]=='^')
        {
            lungime=lungime-1;
            if(c[i+1]=='(')
            {
                lungime=lungime-2;
                v=post_pp(c,n,i+1,r);
                for(int k=0; k<v.length(); k++)
                {
                    if(v[k]=='^' || v[k]=='/')
                    {
                        l2=v.length();
                        a2=dim_formula(v,l2,lungime);
                        break;

                    }
                }
            }
        }
        i--;
    }
    return lungime;
}

void repr(string text,int j,int poz1,int poz2)
{
    char e[50];
    while (j<text.length())
    {
        e[0]=text[j];
        int x=0,y=poz2;
        x=poz1+15*j;
        outtextxy(x,y,e);
        j++;
    }

}

void desen(string c,int j,int poz1, int poz2,int dif)
{
    char e[50];
    string text;
    while (j<c.length())
    {
        int x=0,y,n=0,l1=0,l2=0,l=0,a1=0,a2=0,m=0;
        string r,inainte,dupa;
        e[0]=c[j];
        y=poz2;

        if(isdigit(e[0]) || e[0]=='.' )
        {
            text=text+e[0];
            if(e[0]=='.')
            {
                dif=dif+0.5;
            }
        }
        if(e[0]=='+' || e[0]=='-'|| e[0]=='*' || isalpha(e[0]))
        {
            if(e[0]=='s' && c[j+1]=='q')
            {
                cout<<j;
                inainte=post_pp(c,n,j+4,r);
                l1=inainte.length();
                a1=dim_formula(inainte,l1,l1);
                l1=l1+2;
                x=poz1+15*(j+1-dif);

                if(m_formula(inainte,inainte.length(),m)==0)
                {
                    line(x, y, x+7, y);
                    line(x+7, y, x+10, y+20);
                    line(x+10, y+20, x+17, y-10);
                    line(x+17, y-10, x+17+a1*15+7, y-10);
                    desen(inainte,0,x+5,y,dif);
                    dif=dif+4;
                }
                if(m_formula(inainte,inainte.length(),m)==1)
                {
                    line(x, y, x+7, y);
                    line(x+7, y, x+10, y+40);
                    line(x+10, y+40, x+17, y-25);
                    line(x+17, y-25, x+17+a1*15+15, y-25);
                    desen(inainte,0,x,y,dif);
                    dif=dif+l1-a1+1;
                }
                text="";
                j=j+3+l1;
                cout<<j;
            }

            if(text!="")
            {
                x=poz1+15*(j+1-dif);
                repr(text,0,x-15*(text.length()),y);
                text="";
            }
            else
            {
                x=poz1+15*(j+1-dif);
            }
            outtextxy(x,y,e);
        }
        if(e[0]=='(' || e[0]=='/' || e[0]=='^')
        {
            cout<<"yes1";
            if(e[0]=='(')
            {
                cout<<"yes2";
                inainte=post_pp(c,n,j,r);
                l1=inainte.length();
                a1=dim_formula(inainte,l1,l1);
                l1=l1+2;
            }
            if(e[0]=='/' || e[0]=='^')
            {
                cout<<"yes3";
                text="";
                if(c[j+l1-1]!=')')
                {
                    int h=j+l1-1;
                    while(!strchr(Op,c[h]) && h>=0)
                    {
                        inainte=c[h]+inainte;
                        a1++;
                        h--;
                    }
                    l1=a1;
                }
            }

            if((e[0]=='(' && c[j+l1]=='/') || e[0]=='/' || (e[0]=='(' && c[j+l1]=='^') || e[0]=='^')
            {
                cout<<"yes4";
                if(c[j+l1+1]=='(' || c[j+1]=='(')
                {
                    cout<<"yes5";
                    if(c[j+l1+1]=='(')
                    {
                        dupa=post_pp(c,n,j+l1+1,r);
                    }
                    if(c[j+1]=='(')
                    {
                        dupa=post_pp(c,n,j+1,r);
                    }
                    l2=dupa.length();
                    a2=dim_formula(dupa,l2,l2);
                    l2=l2+2;
                }
                if((c[j+l1+1]!='(' && c[j+l1]=='/') || (e[0]=='/' && c[j+1]!='(') || (c[j+l1+1]!='(' && c[j+l1]=='^') || (e[0]=='^' && c[j+1]!='('))
                {
                    cout<<"yes6";
                    int h;
                    if((c[j+l1+1]!='(' && c[j+l1]=='/') || (c[j+l1+1]!='(' && c[j+l1]=='^'))
                    {
                        h=j+l1+1;
                    }
                    if((e[0]=='/' && c[j+1]!='(' ) || (e[0]=='^' && c[j+1]!='('))
                    {
                        h=j+1;
                    }
                    while(!strchr(Op,c[h]) && h< c.length())
                    {
                        dupa=dupa+c[h];
                        a2++;
                        h++;
                    }
                    l2=a2;
                }
            }
        }
        if((e[0]=='(' && c[j+l1]=='/') || e[0]=='/')
        {
            cout<<"yes7";
            l=l1+l2;
            y=y+8;
            if(c[j+l1]=='/')
            {
                j=j;
            }
            if(e[0]=='/')
            {
                j=j-l1;
            }
            text="";
            x=poz1+15*(j+2-dif);
            if(a1>=a2)
            {
                line(x-7, y, x+15*a1+7, y);
            }
            else
            {
                line(x-7, y, x+15*a2+7, y);
            }
            if(m_formula(dupa,dupa.length(),m)==0 )
            {
                desen(dupa,0,x-15,y+10,dif);
            }
            else
            {
                desen(dupa,0,x-30,y+25,dif);
            }
            if(m_formula(inainte,inainte.length(),m)==0)
            {
                desen(inainte,0,x-15,y-25,dif);
            }
            else
            {
                desen(inainte,0,x-30,y-45,dif);
            }
            j=j+l;
            if(a1>=a2)
            {
                dif=dif+l-a1-1;
            }
            else
            {
                dif=dif+l-a2-1;
            }
            text="";
        }
        if((e[0]=='(' && c[j+l1]=='^') || e[0]=='^' || (e[0]=='(' && (c[j+l1]=='+' || c[j+l1]=='-' || c[j+l1]=='*' || c.length()-(j+l1)==0)))
        {
            cout<<"yes8";
            l=l1+l2;
            if(e[0]=='(')
            {
                cout<<"yes9";
                if(text!="")
                {
                    x=poz1+15*(j+1-dif);
                    repr(text,0,x-15*(text.length()),y);
                    text="";
                }
                else
                {
                    x=poz1+15*(j+1-dif);
                }
                int p=0;
                p=1+6*m_formula(inainte,inainte.length(),m);
                if(p>10)
                {
                    p=10;
                }
                settextstyle(5,0,p);
                outtextxy(x,y-2*p,"(");
                x=poz1+15*(j+a1+2-dif);
                if(m_formula(inainte,inainte.length(),m)==0 || m_formula(inainte,inainte.length(),m)==1)
                {
                    outtextxy(x+2*p,y-2*p,")");
                }
                else
                {
                    outtextxy(x+4*p,y-2*p,")");
                }
                if(p==10)
                {
                    x=poz1+15*(j+2-dif);
                }
                else
                {
                    x=poz1+15*(j+1-dif);
                }
                settextstyle(5,0,1);
                desen(inainte,0,x,y,dif);
                text="";
                y=y-12*m_formula(inainte,inainte.length(),m);
            }
            if(e[0]=='^')
            {
                cout<<"yes10";
                x=poz1+15*(j+1-dif);
                repr(inainte,0,x-15*(inainte.length()),y);
                text="";
            }
            if(c[j+l1]=='^' || e[0]=='^')
            {
                cout<<"yes11";
                dif=dif+1;
                if(e[0]=='^')
                {
                    x=poz1+15*(j+2-dif);
                }
                else
                {
                    x=poz1+15*(j+l1+2-dif);
                }
                if(m_formula(dupa,dupa.length(),m)==0)
                {
                    desen(dupa,0,x,y-10,dif);
                }
                if(m_formula(dupa,dupa.length(),m)==1)
                {
                    int frac=0;
                    for(int k=0; k<dupa.length(); k++)
                    {
                        if(dupa[k]=='/' )
                        {
                            frac++;
                            break;
                        }
                    }
                    if(frac!=0)
                    {
                        y=y-40;
                        x=x+10;
                    }
                    else
                    {
                        y=y-10;
                    }
                    desen(dupa,0,x,y,dif);
                }
                text="";
                j=j+l;
            }
            else
            {
                j=j+l-1;
            }



        }




        if(c.length()-j==1 && text!="")
        {
            x=poz1+15*(j+2-dif);
            repr(text,0,x-15*(text.length()),y);
            text="";
        }
        j++;
    }

}



int main()
{
    string a,c;
    cout << ("introduceti sir= ");
    cin>>a;
    c=a;


    char Op[200]="+-/*^()";
    int m=0,i,lungime;
    i=lungime=c.length();


#define MAX_INPUT_LEN 100
    int input_pos = 0;
    int the_end = 0;
    char text[MAX_INPUT_LEN];
    char e;

    int height=600, width=800;
    initwindow(width,height);



    int j=0,x,y,dif=0;
    //y=(height-100)/m_formula(c,i,m);
    //x=700/dim_formula(c,i,lungime);
    //cout<<x;
    //cout<<y;
    x=50;
    y=250;
    settextstyle(5,0,1);
    desen(c,j,x,y,dif);





    /*   while (!the_end)
       {
           outtextxy (300,200, text);
           e = getch();
           switch (e)
           {
           case 8: // backspace
               if (input_pos)
               {
                   input_pos--;
                   text[input_pos] = 0;
               }
               break;
           case 13: // enter
               the_end = 1;
               break;
           case 27: // Escape = Abort
               text[0] = 0;
               the_end = 1;
               break;
           default:
               if (input_pos < MAX_INPUT_LEN-1 && e >= ' ' && e <= '~')
               {
                   text[input_pos] = e;
                   input_pos++;
                   text[input_pos] = 0;
               }
           }

       }




    */

    getch();
    closegraph();
    return 0;



    return 0;
}





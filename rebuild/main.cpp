#include <iostream>
#include <string>
#include <string.h>
#include <graphics.h>
#include <winbgim.h>
#include <bits/stdc++.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <stdio.h>
#include <time.h>
#include <cmath>
#define MAX 200
#define MAX_INPUT_LEN 100

using namespace std;
char Op[200] = "+-/*^()";

struct token //Robu Victor
{
    string tok;
    string type;
};

struct vectorToken //Robu Victor
{
    token vec[MAX];
};

struct node //Robu Victor
{
    token value;
    node* left, *right;
};

typedef node * arbore; //Robu Victor

bool isBinaryOperator(char character) //Both
{
    if(strchr("+-*/^",character) != NULL)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool isWrongCharacter(char character) //Both
{
    if (strchr(",@#$%{}[]&|\\",character) != NULL)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool isDigit(char character) //Both
{
    if((character>='0') && (character<='9'))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool isLetter(char character) //Both
{
    if ((character >= 'a') && (character <= 'z'))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool isUpperLetter(char character) //Both
{
    if((character >= 'A') && (character <='Z'))
    {
        return true;
    }
    else
    {
        return false;
    }
}

char toLowerCase(char character) //Robu Victor
{
    int ASCIIValue;
    char result;
    ASCIIValue = character;
    result = ASCIIValue + 32;
    return result;
}

bool validateSinus(char first, char second) //Both
{
    if((first == 'i') && (second == 'n'))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool validateSquareRoot(char first, char second, char third) //Both
{
    if((first == 'q') && (second == 'r') && (third == 't'))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool validateCosinus(char first, char second) //Both
{
    if((first == 'o') && (second == 's'))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool validateCotagent(char first, char second) //Both
{
    if((first == 't') && (second == 'g'))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool validateNaturalLogarithm(char first) //Both
{
    if(first == 'n')
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool validateTangent(char first) //Both
{
    if(first == 'g')
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool validateUnaryOperators(string sequence) //Both
{
    bool valid;
    for(int i = 0; i < sequence.length() - 1; i++)
    {
        valid = true;
        switch(sequence[i])
        {
        case 's' :
            if(!isBinaryOperator(sequence[i+1]) && (sequence[i+1] != ')'))
            {
                if(!validateSinus(sequence[i+1], sequence[i+2]))
                {
                    if(!validateSquareRoot(sequence[i+1],sequence[i+2],sequence[i+3]))
                    {
                        valid = false;
                    }
                    else
                    {
                        i = i + 4;
                    }
                }
                else
                {
                    i = i + 3;
                }
            }
            else
            {
                i = i + 1;
            }
            break;
        case 'c' :
            if(!isBinaryOperator(sequence[i+1]) && (sequence[i+1] != ')'))
            {
                if(!validateCosinus(sequence[i+1], sequence[i+2]))
                {
                    if(!validateCotagent(sequence[i+1],sequence[i+2]))
                    {
                        valid = false;
                    }
                    else
                    {
                        i = i + 3;
                    }
                }
                else
                {
                    i = i + 3;
                }
            }
            else
            {
                i = i + 1;
            }
            break;
        case 'l' :
            if(!isBinaryOperator(sequence[i+1]) && (sequence[i+1] != ')'))
            {
                if(!validateNaturalLogarithm(sequence[i+1]))
                {
                    valid = false;
                }
                else
                {
                    i = i + 2;
                }
            }
            else
            {
                i = i + 1;
            }
            break;
        case 't' :
            if(!isBinaryOperator(sequence[i+1]) && (sequence[i+1] != ')'))
            {
                if(!validateTangent(sequence[i+1]))
                {
                    valid = false;
                }
                else
                {
                    i = i + 2;
                }
            }
            else
            {
                i = i + 1;
            }
            break;
        }
        if(valid == false)
        {
            return false;
        }

    }
    return true;
}

bool validateCharacters(string sequence) //Both
{
    for(int i = 0; i < sequence.length(); i++)
    {
        if(isWrongCharacter(sequence[i]))
        {
            return false;
        }
    }
    return true;
}

bool validateLetters(string sequence) //Both
{
    for(int i = 0; i < sequence.length(); i++)
    {
        if(isUpperLetter(sequence[i]))
        {
            return false;
        }
    }
    return true;
}

bool validateStringStartEnd(string sequence) //Both
{
    if((!isLetter(sequence[0])) && (!isDigit(sequence[0])) && (sequence[0] != '('))
    {
        return false;
    }
    if(sequence[0] == ')')
    {
        return false;
    }
    if(sequence[sequence.length() - 1] == '(')
    {
        return false;
    }
    if(sequence[0] == '.')
    {
        return false;
    }
    if(sequence[sequence.length() - 1] == '.')
    {
        return false;
    }
    if(isWrongCharacter(sequence[0]))
    {
        return false;
    }
    if(isWrongCharacter(sequence[sequence.length() - 1]))
    {
        return false;
    }
    if(isBinaryOperator(sequence[0]))
    {
        return false;
    }
    if(isBinaryOperator(sequence[sequence.length() - 1]))
    {
        return false;
    }
    return true;
}

bool validateBrackets(string sequence) //Both
{
    int open, close;
    open = 0;
    close = 0;

    for(int i = 0; i < sequence.length(); i++)
    {
        if(sequence[i] == '(')
        {
            open++;
        }
        if(sequence[i] == ')')
        {
            close++;
        }
    }
    if(open == close)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool checkSpaces(string sequence) //Both
{
    for(int i = 0; i < sequence.length(); i++)
    {
        if(sequence[i] == ' ')
        {
            return false;
        }

    }
    return true;
}

bool checkEmptyBrackets(string sequence) //Both
{
    for(int i = 0; i < sequence.length() - 1; i++)
    {
        if((sequence[i] == '(') && (sequence[i+1] == ')'))
        {
            return true;
        }
    }
    return false;
}

bool validateOpenBrackets(string sequence) //Both
{
    bool valid;
    for(int i = 0; i < sequence.length() - 1; i++)
    {
        if(sequence[i] == '(')
        {
            valid = true;
            switch(sequence[i+1])
            {
            case '*' :
                valid = false;
                break;
            case '/' :
                valid = false;
                break;
            case '^' :
                valid = false;
                break;
            case '.' :
                valid = false;
                break;
            }
            if(valid == false)
            {
                return false;
            }
        }
    }
    return true;
}

bool validateExistingPoints(string sequence) //Both
{
    for(int i = 0; i < sequence.length(); i++)
    {
        if(sequence[i] == '.')
        {
            if((!isDigit(sequence[i-1])) || (!isDigit(sequence[i+1])))
            {
                return false;
            }

        }
    }
    return true;
}

bool validateCloseBrackets(string sequence) //Both
{
    for(int i = 0; i < sequence.length() - 1; i++)
    {
        if(sequence[i] == ')')
        {
            if(sequence[i+1] == '(')
            {
                return false;
            }
            if(isDigit(sequence[i+1]))
            {
                return false;
            }
            if(isLetter(sequence[i+1]))
            {
                return false;
            }
            if(sequence[i+1] == '.')
            {
                return false;
            }
        }
    }
    return true;
}

bool validateNextOperator(string sequence) //Both
{
    for(int i = 0; i < sequence.length() - 1; i++)
    {
        if((isBinaryOperator(sequence[i])) && (isBinaryOperator(sequence[i+1])))
        {
            return false;
        }
    }
    return true;
}

bool validateVar(string sequence) //Both
{
    for(int i = 0; i < sequence.length() - 1; i++)
    {
        if((isDigit(sequence[i])) && (isLetter(sequence[i+1])))
        {
            return false;
        }
        if((isLetter(sequence[i])) && (isDigit(sequence[i+1])))
        {
            return false;
        }
    }
    return true;
}

bool validateNumbers(string sequence) //Both
{
    for(int i = 0; i < sequence.length(); i++)
    {
        if(isDigit(sequence[i]))
        {
            int pointCounter = 0;
            while(((isDigit(sequence[i])) || (sequence[i] == '.')))
            {
                if(sequence[i] == '.')
                {
                    pointCounter++;
                }
                if(pointCounter>1)
                {
                    return false;
                }
                i++;
            }
        }
    }
    return true;
}

bool validateNumbersInput(string sequence) // Robu Victor
{
    if((sequence[0] != '-') && (!isDigit(sequence[0])))
    {
        return false;
    }
    for(int i = 1; i < sequence.length(); i++)
    {
        if((sequence[i] != '.') && (!isDigit(sequence[i])))
        {
            return false;
        }
    }
    return true;
}

bool validateStringAtCharLevel(string sequence) //Both
{
    if(!validateStringStartEnd(sequence))
    {
        return false;
    }
    else if(!validateBrackets(sequence))
    {
        return false;
    }
    else if(!validateOpenBrackets(sequence))
    {
        return false;
    }
    else if(!validateCloseBrackets(sequence))
    {
        return false;
    }
    else if(!validateNextOperator(sequence))
    {
        return false;
    }
    else if(checkEmptyBrackets(sequence))
    {
        return false;
    }
    else if(!validateVar(sequence))
    {
        return false;
    }
    else if(!checkSpaces(sequence))
    {
        return  false;
    }
    else if(!validateCharacters(sequence))
    {
        return false;
    }
    else if(!validateUnaryOperators(sequence))
    {
        return false;
    }
    else if(!validateExistingPoints(sequence))
    {
        return false;
    }
    else if(!validateNumbers(sequence))
    {
        return false;
    }
    else if(!validateLetters(sequence))
    {
        return false;
    }
    else
    {
        return true;
    }
}

vectorToken tokenize(string sequence, int &elementNumber) //Robu Victor
{
    vectorToken tokenized;
    int k = 0;
    string input = sequence;
    int i = 0;

    while(i < input.length())
    {
        if(isLetter(input[i]))
        {
            //verificam daca e operator literal sau variabila, tokenizam
            switch(input[i])
            {
            case 's' :
                if((input[i+1] == 'i') && (input[i+2] == 'n'))
                {
                    tokenized.vec[k].tok = "sin";
                    tokenized.vec[k].type = "UnaryOperator";
                    i = i + 3;
                    k++;
                }
                else if((input[i+1] == 'q') && (input[i+2] == 'r') && (input[i+3] == 't'))
                {
                    tokenized.vec[k].tok = "sqrt";
                    tokenized.vec[k].type = "UnaryOperator";
                    i = i + 4;
                    k++;
                }
                else if(isBinaryOperator(input[i+1]) || (input[i+1] == ')'))
                {
                    tokenized.vec[k].tok = "s";
                    tokenized.vec[k].type = "Variable";
                    i = i + 1;
                    k++;
                }
                break;
            case 'c' :
                if((input[i+1] == 'o') && (input[i+2] == 's'))
                {
                    tokenized.vec[k].tok = "cos";
                    tokenized.vec[k].type = "UnaryOperator";
                    i = i + 3;
                    k++;
                }
                else if((input[i+1] == 't') && (input[i+2] == 'g'))
                {
                    tokenized.vec[k].tok = "ctg";
                    tokenized.vec[k].type = "UnaryOperator";
                    i = i + 3;
                    k++;
                }
                else if(isBinaryOperator(input[i+1]) || (input[i+1] == ')'))
                {
                    tokenized.vec[k].tok = "c";
                    tokenized.vec[k].type = "Variable";
                    i = i + 1;
                    k++;
                }
                break;
            case 'l' :
                if(input[i+1] == 'n')
                {
                    tokenized.vec[k].tok = "ln";
                    tokenized.vec[k].type = "UnaryOperator";
                    i = i + 2;
                    k++;
                }
                else if(isBinaryOperator(input[i+1]) || (input[i+1] == ')'))
                {
                    tokenized.vec[k].tok = "l";
                    tokenized.vec[k].type = "Variable";
                    i = i + 1;
                    k++;
                }
                break;
            case 't' :
                if(input[i+1] == 'g')
                {
                    tokenized.vec[k].tok = "tg";
                    tokenized.vec[k].type = "UnaryOperator";
                    i = i + 2;
                    k++;
                }
                else if(isBinaryOperator(input[i+1]) || (input[i+1] == ')'))
                {
                    tokenized.vec[k].tok = "t";
                    tokenized.vec[k].type = "Variable";
                    i = i + 1;
                    k++;
                }
                break;
            default :
                tokenized.vec[k].tok = input[i];
                tokenized.vec[k].type = "Variable";
                i++;
                k++;
            }
        }
        else
        {
            //verificam daca e numar oarecare, tokenizam
            if(isDigit(input[i]))
            {
                int pointCounter = 0;
                string number = "";
                while(((isDigit(input[i])) || (input[i] == '.')))
                {
                    if(input[i] == '.')
                    {
                        pointCounter++;
                    }
                    if(pointCounter<=1)
                    {
                        number += input[i];
                        i++;
                    }
                }
                tokenized.vec[k].tok = number;
                tokenized.vec[k].type = "Number";
                k++;
            }
            else
            {
                //verificam daca e paranteza deschisa, tokenizam
                if(input[i] == '(')
                {
                    tokenized.vec[k].tok = "(";
                    tokenized.vec[k].type = "OpenBrackets";
                    k++;
                    i++;
                }
                else
                {
                    //verificam daca e paranteza inchisa, tokenizam
                    if(input[i] == ')')
                    {
                        tokenized.vec[k].tok = ")";
                        tokenized.vec[k].type = "CloseBrackets";
                        k++;
                        i++;
                    }
                    else
                    {
                        switch(input[i])
                        {
                        case '+' :
                            tokenized.vec[k].tok ="+";
                            tokenized.vec[k].type = "BinaryOperator";
                            k++;
                            i++;
                            break;
                        case '-' :
                            tokenized.vec[k].tok ="-";
                            tokenized.vec[k].type = "BinaryOperator";
                            k++;
                            i++;
                            break;
                        case '*' :
                            tokenized.vec[k].tok ="*";
                            tokenized.vec[k].type = "BinaryOperator";
                            k++;
                            i++;
                            break;
                        case '/' :
                            tokenized.vec[k].tok ="/";
                            tokenized.vec[k].type = "BinaryOperator";
                            k++;
                            i++;
                            break;
                        case '^' :
                            tokenized.vec[k].tok ="^";
                            tokenized.vec[k].type = "BinaryOperator";
                            k++;
                            i++;
                            break;
                        }
                    }
                }
            }
        }
    }
    elementNumber = k;
    return tokenized;
}

int precedence(string c) //Both
{
    if((c == "sin") || (c == "cos") || (c == "ln") || (c == "tg") || (c == "sqrt") || (c == "ctg") || (c == "exp"))
        return 4;
    else if(c == "^")
        return 3;
    else if(c == "/" || c == "*")
        return 2;
    else if(c == "+" || c == "-")
        return 1;
    else
        return -1;
}

vectorToken infixToPostfix(vectorToken s, int numberOfElements, int &postfixElements) //Robu Victor
{
    stack<token> st; //For stack operations, we are using C++ built in stack
    vectorToken result;
    int counter = -1;
    for(int i = 0; i < numberOfElements; i++)
    {

        // If the scanned character is
        // an operand, add it to output structure.
        if((s.vec[i].type == "Variable") || (s.vec[i].type == "Number"))
        {
            counter++;
            result.vec[counter] = s.vec[i];
        }

        // If the scanned character is an
        // �(�, push it to the stack.
        else if(s.vec[i].tok == "(")
        {
            st.push(s.vec[i]);
        }
        // If the scanned character is an �)�,
        // pop and to output string from the stack
        // until an �(� is encountered.
        else if(s.vec[i].tok == ")")
        {
            while(st.top().tok != "(")
            {
                counter++;
                result.vec[counter] = st.top();
                st.pop();
            }
            st.pop();
        }

        //If an operator is scanned
        else
        {
            while(!st.empty() && precedence(s.vec[i].tok) <= precedence(st.top().tok))
            {
                counter++;
                result.vec[counter] = st.top();
                st.pop();
            }
            st.push(s.vec[i]);
        }
    }

    // Pop all the remaining elements from the stack
    while(!st.empty())
    {
        counter++;
        result.vec[counter] = st.top();
        st.pop();
    }
    postfixElements = counter + 1;
    return result;
}

string unarMinus(string input) //Robu Victor
{
    for(int i = 0; i < input.length(); i++)
    {
        if((input[i] == '(') && (input[i+1] == '-'))
        {
            input.insert(i + 1, "0");
        }
    }
    return input;
}

string unarPlus(string input) //Robu Victor
{
    for(int i = 0; i < input.length(); i++)
    {
        if((input[i] == '(') && (input[i+1] == '+'))
        {
            input.insert(i + 1, "0");
        }
    }
    return input;
}

string tryFixEmptyBrackets(string sequence) //Robu Victor
{
    for(int i = 0; i < sequence.length(); i++)
    {
        if((sequence[i] == '(') && (sequence[i+1] == ')'))
        {
            sequence.erase(i,2);
        }
    }
    return sequence;
}

string tryFixSpaces(string sequence) //Robu Victor
{
    for(int i = 0; i < sequence.length(); i++)
    {
        if(sequence[i] == ' ')
        {
            sequence.erase(sequence.begin() + i);
        }
    }
    return sequence;
}

string tryFixWrongCharacters(string sequence) //Robu Victor
{
    for(int i = 0; i < sequence.length(); i++)
    {
        if(isWrongCharacter(sequence[i]))
        {
            sequence.erase(sequence.begin() + i);
        }
    }
    return sequence;
}

string tryFixUpperLetters(string sequence) //Robu Victor
{
    for(int i = 0; i < sequence.length(); i++)
    {
        if(isUpperLetter(sequence[i]))
        {
            sequence[i] = toLowerCase(sequence[i]);
        }
    }
    return sequence;
}

string tryFixVariables(string sequence) //Robu Victor
{
    for(int i = 0; i < sequence.length(); i++)
    {
        if(isLetter(sequence[i]))
        {
            if(isDigit(sequence[i+1]))
            {
                i++;
                while((isDigit(sequence[i])) || (isLetter(sequence[i])))
                {
                    sequence.erase(sequence.begin() + i);
                }
            }
        }
        else
        {
            if(isDigit(sequence[i]))
            {
                if(isLetter(sequence[i+1]))
                {
                    i++;
                    while((isDigit(sequence[i])) || (isLetter(sequence[i])))
                    {
                        sequence.erase(sequence.begin() + i);
                    }
                }

            }
        }
    }
    return sequence;
}

string tryFixBrackets(string sequence) //Robu Victor
{
    string input = sequence;
    int open = 0;
    int close = 0;
    for(int i = 0; i < input.length(); i++)
    {
        if(input[i] == '(')
        {
            open++;
        }
        if(input[i] == ')')
        {
            close++;
        }
    }

    int bracketsDifference = open - close;
    if(bracketsDifference < 0)
    {
        int difference = abs(bracketsDifference);
        for(int i = 0; i < difference; i++)
        {
            input.insert(0,"(");
        }
    }
    else if(bracketsDifference > 0)
    {
        for(int i = 0; i < bracketsDifference; i++)
        {
            input.insert(input.length(),")");
        }
    }
    return input;
}

string tryFixExistingPoints(string sequence) //Robu Victor
{
    for(int i = 0; i < sequence.length(); i++)
    {
        if(sequence[i] == '.')
        {
            if((!isDigit(sequence[i+1])) || (!isDigit(sequence[i-1])))
            {
                sequence.erase(sequence.begin() + i);
            }

        }
    }
    return sequence;
}

string tryFixNumbers(string sequence) //Robu Victor
{
    for(int i = 0; i < sequence.length(); i++)
    {
        if(isDigit(sequence[i]))
        {
            int pointCounter = 0;
            while((isDigit(sequence[i])) || (sequence[i] == '.'))
            {
                if(sequence[i] == '.')
                {
                    pointCounter++;
                }
                if(pointCounter > 1)
                {
                    while((!isBinaryOperator(sequence[i])) && (sequence[i] != ')'))
                    {
                        if(sequence[i] == '.')
                        {
                            sequence.erase(sequence.begin() + i);
                        }
                        i++;
                    }
                }
                i++;
            }
        }
    }
    return sequence;
}

string tryFixStartEnd(string sequence) //Robu Victor
{
    if(isBinaryOperator(sequence[0]))
    {
        sequence.erase(sequence.begin() + 0);
    }
    if(isBinaryOperator(sequence[sequence.size() - 1]))
    {
        sequence.erase(sequence.begin() + sequence.size() - 1);
    }
    return sequence;
}

string tryFixAdjacentBinaryOperators(string sequence) //Robu Victor
{
    for(int i = 0; i < sequence.length(); i++)
    {
        if((isBinaryOperator(sequence[i])) && (isBinaryOperator(sequence[i+1])))
        {
            sequence.erase(sequence.begin() + i);
        }
    }
    return sequence;
}

string tryFixUnaryOperators(string sequence) //Robu Victor
{
    for(int i = 0; i < sequence.length(); i++)
    {
        switch(sequence[i])
        {
        case 's' :
            if((isLetter(sequence[i+1])) && (isLetter(sequence[i+2])))
            {
                if(sequence[i+3] == '(')
                {
                    sequence[i+1] = 'i';
                    sequence[i+2] = 'n';
                }
                else
                {
                    if((isLetter(sequence[i+3])) && (sequence[i+4] == '('))
                    {
                        sequence[i+1] = 'q';
                        sequence[i+2] = 'r';
                        sequence[i+3] = 't';
                    }
                }
            }
            break;
        case 'c' :
            if(isLetter(sequence[i+1]) && (isLetter(sequence[i+2])) && (sequence[i+3] == '('))
            {
                if((sequence[i+1] == 'o') || (sequence[i+2] == 'o') || (sequence[i+1] == 's') || (sequence[i+2] == 's'))
                {
                    sequence[i+1] = 'o';
                    sequence[i+2] = 's';
                }
                else if((sequence[i+1] == 'g') || (sequence[i+2] == 'g') || (sequence[i+1] == 't') || (sequence[i+2] == 't'))
                {
                    sequence[i+1] = 't';
                    sequence[i+2] = 'g';
                }
                else
                {
                    sequence[i+1] = 'o';
                    sequence[i+2] = 's';
                }
            }
            break;
        case 'l' :
            if(isLetter(sequence[i+1]) && (sequence[i+2] == '('))
            {
                sequence[i+1] = 'n';
            }
            break;
        case 't' :
            if(isLetter(sequence[i+1]) && (sequence[i+2] == '('))
            {
                sequence[i+1] = 'g';
            }
            break;
        case 'e' :
            if(isLetter(sequence[i+1]) && (isLetter(sequence[i+2])) && (sequence[i+3] == '('))
            {
                sequence[i+1] = 'x';
                sequence[i+2] = 'p';
            }
            break;
        }
    }
    return sequence;

}

string tryFixExpression(string sequence) //Robu Victor
{
    string fixed;
    fixed = tryFixSpaces(sequence);
    fixed = tryFixWrongCharacters(fixed);
    fixed = tryFixExistingPoints(fixed);
    fixed = tryFixStartEnd(fixed);
    fixed = tryFixUpperLetters(fixed);
    fixed = tryFixUnaryOperators(fixed);
    fixed = tryFixBrackets(fixed);
    fixed = tryFixEmptyBrackets(fixed);
    fixed = tryFixNumbers(fixed);
    fixed = tryFixAdjacentBinaryOperators(fixed);
    fixed = tryFixVariables(fixed);
    return fixed;
}

node* newNode(token v) //Robu Victor
{
    node *temp = new node;
    temp->left = temp->right = NULL;
    temp->value = v;
    return temp;
};

node* constructTree(vectorToken postfix, int number) //Robu Victor
{
    stack<node *> st;
    node *t, *t1, *t2;

    // Traverse through every token of
    // input expression
    for (int i = 0; i < number; i++)
    {
        // If operand, simply push into stack
        if ((postfix.vec[i].type == "Number") || (postfix.vec[i].type == "Variable"))
        {
            t = newNode(postfix.vec[i]);
            st.push(t);
        }
        else // operator
        {
            if(postfix.vec[i].type == "UnaryOperator")
            {
                t = newNode(postfix.vec[i]);

                // Pop one top node
                t1 = st.top(); // Store top
                st.pop();      // Remove top

                //  make it children
                t->left = t1;

                // Add this subexpression to stack
                st.push(t);
            }
            else
            {
                if(postfix.vec[i].type == "BinaryOperator")
                {
                    t = newNode(postfix.vec[i]);

                    // Pop two top nodes
                    t1 = st.top(); // Store top
                    st.pop();      // Remove top
                    t2 = st.top();
                    st.pop();

                    //  make them children
                    t->right = t1;
                    t->left = t2;

                    // Add this subexpression to stack
                    st.push(t);
                }
            }

        }
    }

    //  only element will be root of expression
    // tree
    t = st.top();
    st.pop();

    return t;
}

int widthTree(arbore a) //Robu Victor
{
    if (a == NULL)
        return 0;
    else
    {
        int w1 = widthTree(a->left);
        int w2 = widthTree(a->right);
        return 1 + w1 + w2;
    }
}

int heightTree (arbore a) //Robu Victor
{
    if (a == NULL)
        return 0;
    else
    {
        int h1 = heightTree(a->left);
        int h2 = heightTree(a->right);
        return 1 + max(h1, h2);
    }
}

void drawNode(token elem, int xc, int yc, int width, int height) //Robu Victor
{
    string temp = elem.tok;
    char * tab2 = new char [temp.length()+1];
    strcpy(tab2, temp.c_str());
    setcolor(WHITE);
    setfillstyle(SOLID_FILL,WHITE);
    settextstyle(SANS_SERIF_FONT,HORIZ_DIR,1);
    setbkcolor(WHITE);
    fillellipse(xc,yc,textwidth(tab2)+5, textheight("T")/2+6);
    setcolor(RED);
    settextjustify(1,1);
    outtextxy(xc,yc+4,tab2);
}

void drawTree(node *a, int level, int leftNd, int width, int height) //Robu Victor
{
    if(a!=NULL)
    {
        int n1=widthTree(a->left);
        int xc=leftNd+width*n1+width/2;
        int yc=level*height-height/2;

        if (a->left!=NULL)
        {
            int xcs=leftNd+width*widthTree(a->left->left)+width/2;
            setcolor(WHITE);
            line(xc,yc,xcs,yc+height);
        }
        if (a->right!=NULL)
        {
            int xcd=leftNd+width*(n1+1)+width*widthTree(a->right->left)+width/2;
            setcolor(WHITE);
            line(xc,yc,xcd,yc+height);
        }
        drawNode(a->value, xc, yc, width, height);
        drawTree(a->left, level+1, leftNd, width, height);
        drawTree(a->right, level+1, leftNd+width*(n1+1), width, height);
    }
}

void inorder(node *t) //Robu Victor
{
    if(t)
    {
        inorder(t->left);
        //cout << t->value.tok;
        inorder(t->right);
    }
}

int treeTextWidth(node *a) //Robu Victor
{
    if(a == NULL)
    {
        return 0;
    }
    else
    {
        if(a->value.tok == "^")
        {
            int t1 = treeTextWidth(a->left);
            int t2 = treeTextWidth(a->right);
            return t1 + t2 / 2;
        }
        if(a->value.tok == "/")
        {
            int t1 = treeTextWidth(a->left);
            int t2 = treeTextWidth(a->right);
            return max(t1, t2);
        }
        string temp = a->value.tok;
        char * tab2 = new char [temp.length()+1];
        strcpy(tab2, temp.c_str());
        int txtWidth = textwidth(tab2);
        int t1 = treeTextWidth(a->left);
        int t2 = treeTextWidth(a->right);
        return txtWidth + t1 + t2;
    }
}

int treeTextHeight(node *a) //Robu Victor
{
    if(a == NULL)
    {
        return 0;
    }
    else
    {
        if(a->value.tok == "^")
        {
            int t1 = treeTextHeight(a->left);
            int t2 = treeTextHeight(a->right);
            return t1 + t2 / 2;
        }
        if(a->value.tok == "/")
        {
            int t1 = treeTextWidth(a->left);
            int t2 = treeTextWidth(a->right);
            return textheight("-") + t1 + t2;
        }
        string temp = a->value.tok;
        char * tab2 = new char [temp.length()+1];
        strcpy(tab2, temp.c_str());
        int txtHeight = textheight(tab2);
        int t1 = treeTextHeight(a->left);
        int t2 = treeTextHeight(a->right);
        return max(max(t1, t2), txtHeight);
    }
}

float squareRootSelf(float x) //Robu Victor
{
    float r1 = 1, rn, precedent;
    float eps = 0.000001;
    if (x == 0)
    {
        return 0;
    };
    do
    {
        rn = 0.5*(r1+x/r1);
        precedent = r1;
        if (abs(rn - r1) <= eps)
        {
            return rn;
        }
        else
        {
            r1 = rn;
        };
    }
    while(abs(rn - precedent) > eps);
    return rn;
}

double cosinusSelf(double x) //Robu Victor
{
    double an = 1.0, cosx,eps = 0.000001;
    double factorial = 1.0, precedent;
    int i = 2;
    if (x == 0)
    {
        return 1;
    };
    int semn = -1;
    do
    {
        factorial = factorial * (i - 1) * i;
        cosx = an + semn * ((pow(x,i)) / (factorial));
        i = i + 2;

        if (semn == -1)
        {
            semn = 1;
        }
        else
        {
            if (semn == 1)
            {
                semn = -1;
            }
        };

        precedent = an;

        if (abs(cosx - an) <= eps)
        {
            return cosx;
        }
        else
        {
            an = cosx;
        };
    }
    while ((abs(cosx - precedent)) > eps);
    return cosx;
}

double sinusSelf(double x) //Robu Victor
{
    double an = x, sinx,eps = 0.000001;
    double factorial = 1.0, precedent;
    int i = 3;
    if (x == 0)
    {
        return 0;
    };
    int semn = -1;
    do
    {
        factorial = factorial * (i - 1) * i;
        sinx = an + semn *((pow(x, i)) / (factorial));
        i = i + 2;


        if (semn == -1)
        {
            semn = 1;
        }
        else
        {
            if (semn == 1)
            {
                semn=-1;
            }
        };

        precedent = an;

        if (abs(sinx - an) <= eps)
        {
            return sinx;
        }
        else
        {
            an = sinx;
        };

    }
    while ((abs(sinx - precedent)) > eps);
    return sinx;
}

void ReadInput(char text[MAX_INPUT_LEN], int x, int y);

string expressionValue(vectorToken postfixTokenized, int elementNumber) //Robu Victor
{
    stack<string> operands;
    int varNumber = 0;


    for(int i = 0; i < elementNumber; i++)
    {
        if(postfixTokenized.vec[i].type == "Number")
        {
            operands.push(postfixTokenized.vec[i].tok);
        }
        else if(postfixTokenized.vec[i].type == "Variable")
        {
            string first = "Input variable ";
            string second = postfixTokenized.vec[i].tok;
            string third = " value : ";
            string general = first + second + third;
            char * tab2 = new char [general.length()+1];
            strcpy(tab2, general.c_str());
            outtextxy(100, 400 + 15 * varNumber, tab2);
            char text[MAX_INPUT_LEN]="";
            ReadInput(text, 330, 400 + 15 * varNumber);
            string inputString = text;
            varNumber++;
            if((validateNumbersInput(inputString)) && (validateNumbers(inputString)))
            {
                int j = i + 1;
                while(j < elementNumber)
                {
                    if(postfixTokenized.vec[j].tok == postfixTokenized.vec[i].tok)
                    {
                        postfixTokenized.vec[j].tok = inputString;
                        postfixTokenized.vec[j].type = "Number";
                    }
                    j++;
                }
                operands.push(inputString);
            }
            else
            {
                return "Calculus Error. Check Input";
            }
        }
        else if(postfixTokenized.vec[i].type == "UnaryOperator")
        {
            string operandOneString = operands.top();
            operands.pop();
            double operandOne = stod(operandOneString);
            double valueToken;
            if(postfixTokenized.vec[i].tok == "sin")
            {
                valueToken = sin(operandOne);
            }
            else if(postfixTokenized.vec[i].tok == "cos")
            {
                valueToken = cos(operandOne);
            }
            else if(postfixTokenized.vec[i].tok == "sqrt")
            {
                if(operandOne < 0)
                {
                    return "Calculus Error. Check Input";
                }
                else
                {
                    valueToken = squareRootSelf(operandOne);
                }
            }
            else if(postfixTokenized.vec[i].tok == "ctg")
            {
                if(operandOne == 0)
                {
                    return "Calculus Error. Check Input";
                }
                else
                {
                    valueToken = 1.0 / tan(operandOne);
                }
            }
            else if(postfixTokenized.vec[i].tok == "ln")
            {
                if(operandOne <= 0)
                {
                    return "Calculus Error. Check Input";
                }
                else
                {
                    valueToken = log(operandOne);
                }
                }
            else if(postfixTokenized.vec[i].tok == "tg")
            {
                valueToken = tan(operandOne);
            }
            string valueString = to_string(valueToken);
            operands.push(valueString);
        }
        else if(postfixTokenized.vec[i].type == "BinaryOperator")
        {
            string operandOneString = operands.top();
            operands.pop();
            string operandTwoString = operands.top();
            operands.pop();
            double operandOne = stod(operandOneString);
            double operandTwo = stod(operandTwoString);
            double valueToken;
            if(postfixTokenized.vec[i].tok == "+")
            {
                valueToken = operandOne + operandTwo;
            }
            else if(postfixTokenized.vec[i].tok == "-")
            {
                valueToken = operandTwo - operandOne;
            }
            else if(postfixTokenized.vec[i].tok == "*")
            {
                valueToken = operandOne * operandTwo;
            }
            else if(postfixTokenized.vec[i].tok == "/")
            {
                if(operandOne == 0)
                {
                    return "Calculus Error. Check Input";
                }
                else
                {
                valueToken = operandTwo / operandOne;
                }
            }
            else if(postfixTokenized.vec[i].tok == "^")
            {
                valueToken = pow(operandTwo, operandOne);
            }
            string valueString = to_string(valueToken);
            operands.push(valueString);
        }
    }
    string expression = operands.top();
    operands.pop();
    return expression;

}

string autoGenerateFormula() // Robu Victor
{
    FILE *f;
    char rind[100];
    string expression;
    int counter = -1, randomValue;
    if (!(f = fopen("random.txt", "r")))
    {
        perror("Eroare la deschidere, cu mesajul de eroare:");
        exit(1);
    }
    srand(time(0));
    randomValue = rand() % 11;
    while (!feof(f))
    {
        if (!fgets(rind, 100, f))
        {
            perror("Eroare la citire.\n");
            exit(1);
        }
        counter += 1;
        if (counter == randomValue)
        {
            expression = rind;
        }
    }
    fclose(f);
    expression.resize(expression.size() - 1);
    return expression;

}

string post_pp(string c, int n, int i, string r) //S�rbu Valentina
{
    for(int j = i; j < c.length(); j++)
    {
        if(c[j] != '(' && c[j] != ')')
        {
            r = r + c[j];
        }
        if(c[j] == '(')
        {
            if(n)
            {
                r = r + c[j];
            }
            n = n + 1;
            post_pp(c, n, j + 1, r);
        }
        if(c[j] == ')')
        {
            n = n - 1;
            if(n != 0)
            {
                r = r + c[j];
                post_pp(c, n, j + 1, r);
            }
            if(n == 0)
            {
                return r ;
            }
        }
    }
}

string before_pp(string c, int n, int i, string r) //S�rbu Valentina
{
    for(int j = i; j >= 0; j--)
    {
        if(c[j] != '(' && c[j] != ')')
        {
            r = c[j] + r;
        }
        if(c[j] == ')')
        {
            if(n)
            {
                r = c[j] + r;
            }
            n = n + 1;
            before_pp(c, n, j - 1, r);
        }
        if(c[j] == '(')
        {
            n = n - 1;
            if(n != 0)
            {
                r = c[j] + r;
                before_pp(c, n , j - 1, r);
            }
            if(n == 0)
            {
                return r;
            }
        }
    }
}

int m_formula(string c, int i, int m) //S�rbu Valentina
{
    while(i >= 0)
    {
        int n = 0, a1 = 0, a2 = 0, l1 = 0, l2 = 0;
        string r, v;
        if(c[i] == '/' || c[i] == '^' || (c[i] == 's' && c[i+1] == 'q'))
        {
            if(!m)
            {
                m++;
            }
            if(c[i-1] == ')')
            {
                v = before_pp(c, n, i - 1, r);
                i = i - v.length() - 2;
                for(int k = 0; k < v.length(); k++)
                {
                    if(v[k] == '/' || v[k] == '^')
                    {
                        m++;
                        l1 = v.length();
                        a1 = m_formula(v, l1, m);
                        break;
                    }
                }
            }
            if(c[i+1] == '(' || c[i+4] == '(')
            {
                v = post_pp(c, n, i + 1, r);
                for(int k = 0; k < v.length(); k++)
                {
                    if(v[k] == '/' || v[k] == '^' || (v[k] == 's' && v[k+1] == 'q'))
                    {
                        m++;
                        l2 = v.length();
                        a2 = m_formula(v, l2, m);
                        break;
                    }
                }
            }
        }
        i--;
    }
    return m;
}

int dim_formula(string c,int i,int lungime)
{
    while (i > 0)
    {
        int n = 0,l1 = 0,l2 = 0,a1 = 0,a2 = 0,g = i - 1,h = i + 1;
        string r,v;

        if (c[i] == '/')
        {
            lungime = lungime - 1;
            if (c[i-1] == ')')
            {
                lungime=lungime-2;
                v = before_pp(c, n, i-1, r);
                i = i - v.length() - 2;
                for (int k = 0; k < v.length(); k++)
                {
                    if (v[k] == '^' || v[k] == '/' || (v[k] == 'r' && v[k+1] == 't'))
                    {
                        l1 = v.length();
                        a1 = dim_formula(v, l1, l1);
                        break;
                    }
                    else
                    {
                        a1 = v.length();
                    }
                }
                i = i + v.length() + 2;
            }
            if (c[i-1] != ')')
            {
                while (!strchr(Op,c[g]) && g >= 0)
                {
                    a1++;
                    g--;
                }
            }
            if (c[i+1] == '(')
            {
                lungime = lungime - 2;
                v = post_pp(c, n, i + 1, r);
                for (int k  =0; k  <v.length(); k++)
                {
                    if (v[k] == '^' || v[k] == '/' || (v[k] == 'r' && v[k+1] == 't'))
                    {
                        l2 = v.length();
                        a2 = dim_formula(v, l2, l2);
                        break;
                    }
                    else
                    {
                        a2 = v.length();
                    }
                }
            }
            if (c[i+1] != '(')
            {
                while (!strchr(Op,c[h]) && h < c.length())
                {
                    a2++;
                    h++;
                }
            }
            if (a1 >= a2)
            {
                lungime = lungime - a2;
            }
            else
            {
                lungime = lungime - a1;
            }
        }
        if (c[i] == '^')
        {
            lungime = lungime - 1;
            if (c[i+1] == '(')
            {
                lungime = lungime - 2;
                v = post_pp(c, n, i + 1, r);
                for (int k = 0; k < v.length(); k++)
                {
                    if (v[k] == '^' || v[k] == '/' || (v[k] == 'r' && v[k+1] == 't'))
                    {
                        l2 = v.length();
                        a2 = dim_formula(v, l2, lungime);
                        break;
                    }
                }
            }
        }
        if (c[i] == 'r' && c[i+1] == 't')
        {
            lungime = lungime - 4;
            if (c[i+4] == '(')
            {
                v = post_pp(c, n, i + 4, r);
                for (int k = 0; k < v.length(); k++)
                {
                    if (v[k] == '^' || v[k] == '/' || (v[k] == 'r' && v[k+1] == 't'))
                    {
                        l2 = v.length();
                        a2 = dim_formula(v, l2, lungime);
                        break;
                    }
                }
            }
        }
        i--;
    }
    return lungime;
}


void repr(string text, int j, int poz1, int poz2) //S�rbu Valentina
{
    char e[50] = "";
    while (j < text.length())
    {
        e[0] = text[j];
        int x = 0,y = poz2;
        x = poz1 + 15 * j;
        outtextxy(x, y, e);
        j++;
    }

}

int frac_prin(string c, int j, int lungime) //S�rbu Valentina
{
    int raspuns = 0;
    while(j >= 0)
    {
        int n = 0,l1 = 0;
        string r, paranteza;
        if(c[j] == ')')
        {
            paranteza = before_pp(c, n, j, r);
            l1 = paranteza.length() + 2;
            j = j - l1;
        }
        if(c[j] == '/')
        {
            raspuns = j;
        }
        j--;
    }
    return raspuns;
}

void desen(string c, int j, int poz1, int poz2, int dif) //S�rbu Valentina
{
    char e[50] = "";
    string text;
    while (j < c.length())
    {
        int x = 0, y, n = 0, l1 = 0, l2 = 0, l = 0, a1 = 0, a2 = 0, m = 0;
        string r, inainte, dupa;
        e[0] = c[j];
        y = poz2;
        if(isdigit(e[0]) || e[0] == '.' )
        {
            text = text + e[0];
            if(e[0] == '.')
            {
                dif = dif + 0.5;
            }
        }
        if(e[0] == '+' || e[0] == '-'|| e[0] == '*' || isalpha(e[0]))
        {
            if(text != "")
            {
                x = poz1 + 15 * (j + 1 - dif);
                repr(text, 0, x - 15 * (text.length()), y);
                text = "";
            }
            if(e[0] == 's' && c[j+1] == 'q')
            {
                int p = 0;
                inainte = post_pp(c, n, j + 4, r);
                l1 = inainte.length();
                a1 = dim_formula(inainte, l1, l1);
                a1 = abs(a1);
                l1 = l1 + 5;
                p = m_formula(inainte, inainte.length(), m);
                if(m_formula(inainte, inainte.length(), m) == 0)
                {
                    x = poz1 + 15 * (j + 1 - dif) + 10;
                    line(x, y, x + 7, y);
                    line(x + 7, y, x + 10, y + 20);
                    line(x + 10, y + 20, x + 17, y - 10);
                    line(x + 17, y - 10, x + 17 + a1 * 15 + 7, y - 10);
                    desen(inainte, 0, x + 5, y, 0);
                    dif = dif + l1 - a1 - 1;
                }
                if(m_formula(inainte, inainte.length(), m) != 0)
                {
                    int frac1 = 0, frac2 = 0, frac3 = 0;
                    for(int k = 0; k < inainte.length(); k++)
                    {
                        if(inainte[k] == '/' )
                        {
                            frac1++;
                        }
                    }
                    if(frac1 > 1)
                    {
                        int d = frac_prin(inainte, inainte.length(), inainte.length());
                        for(int k = 0; k < d; k++)
                        {
                            if(inainte[k] == '/' )
                            {
                                frac2++;
                            }
                        }
                        int z = 0,q = 0;
                        if(frac2 == 0)
                        {
                            z = 2;
                        }
                        else
                        {
                            z = 3;
                        }
                        if(frac3 == 0)
                        {
                            q = 3;
                        }
                        else
                        {
                            q = 4;
                        }
                        frac3 = frac1 - frac2 - 1;
                        x = poz1 + 15 * (j + 1 - dif) + 10;
                        line(x, y, x + 7, y);
                        line(x + 7, y, x + 10, y + 16 * (q + frac3));
                        line(x + 10, y + 16 * (q + frac3), x + 17, y - 15 * (frac2 + z) - 5);
                        line(x + 17, y - 15 * (frac2 + z) - 5, x + 17 + (1 + a1) * 15, y - 15 * (frac2 + z) - 5);
                        desen(inainte, 0, x + 3, y, 0);
                        dif = dif + l1 - a1 - 2;
                    }
                    if(frac1 <= 1)
                    {
                        x = poz1 + 15 * (j + 1 - dif) + 10;
                        line(x, y, x + 7, y);
                        line(x + 7, y, x + 10, y + 16 * (p + 1));
                        line(x + 10, y + 16 * (p + 1), x + 17, y - 15 * (p + 1) - 5);
                        line(x + 17, y - 15 * (p + 1) - 5, x + 17 + (a1 + 1) * 15, y - 15 * (p + 1) - 5);
                        desen(inainte, 0, x + 3, y, 0);
                        dif = dif + l1 - a1 - 2;
                    }
                }
                text = "";
                j = j + l1;
                poz1 = poz1 + 10;
            }
            else
            {
                x = poz1 + 15 * (j + 1 - dif);
                outtextxy(x, y, e);
            }
        }
        if(e[0] == '(' || e[0] == '/' || e[0] == '^')
        {
            if(e[0] == '(')
            {
                inainte = post_pp(c, n, j, r);
                l1 = inainte.length();
                a1 = dim_formula(inainte, l1, l1);
                l1 = l1 + 2;
            }
            if(e[0] == '/' || e[0] == '^')
            {
                text = "";
                if(c[j+l1-1] != ')')
                {
                    int h = j + l1 - 1;
                    while(!strchr(Op, c[h]) && h >= 0)
                    {
                        inainte = c[h] + inainte;
                        a1++;
                        h--;
                    }
                    l1 = a1;
                }
            }
            if((e[0] == '(' && c[j+l1] == '/') || e[0] == '/' || (e[0] == '(' && c[j+l1] == '^') || e[0] == '^')
            {
                if(c[j+l1+1] == '(' || c[j+1] == '(')
                {
                    if(c[j+l1+1] == '(')
                    {
                        dupa = post_pp(c, n, j + l1 + 1, r);
                    }
                    if(c[j+1] == '(')
                    {
                        dupa = post_pp(c, n, j + 1, r);
                    }
                    l2 = dupa.length();
                    a2 = dim_formula(dupa, l2, l2);
                    l2 = l2 + 2;
                }
                if((c[j+l1+1] != '(' && c[j+l1] == '/') || (e[0] == '/' && c[j+1] != '(') || (c[j+l1+1] != '(' && c[j+l1] == '^') || (e[0] == '^' && c[j+1] != '('))
                {
                    int h;
                    if((c[j+l1+1] != '(' && c[j+l1] == '/') || (c[j+l1+1] != '(' && c[j+l1] == '^'))
                    {
                        h = j + l1 + 1;
                    }
                    if((e[0] == '/' && c[j+1] != '(' ) || (e[0] == '^' && c[j+1] != '('))
                    {
                        h = j + 1;
                    }
                    while(!strchr(Op, c[h]) && h < c.length())
                    {
                        dupa = dupa + c[h];
                        a2++;
                        h++;
                    }
                    l2 = a2;
                }
            }
        }
        if((e[0] == '(' && c[j+l1] == '/') || e[0] == '/')
        {
            l = l1 + l2;
            y = y + 8;
            if(c[j+l1] == '/')
            {
                j = j;
            }
            if(e[0] == '/')
            {
                j = j - l1;
            }
            text = "";
            x = poz1 + 15 * (j + 2 - dif);
            int egalitate1 = 0,egalitate2 =  0;
            if(a1 >= a2)
            {
                line(x - 7, y, x + 15 * a1 + 5, y);
                egalitate2 = (a1 - a2) / 2;
            }
            else
            {
                line(x - 7, y, x + 15 * a2 + 5, y);
                egalitate1 = (a2 - a1) / 2;
            }
            if(m_formula(dupa, dupa.length(), m) == 0)
            {
                desen(dupa, 0, x - 15 + egalitate2 * 15, y + 10, 0);
            }
            else
            {
                int nivel = m_formula(dupa, dupa.length(), m);
                int frac = 0;
                for(int k = 0; k < dupa.length(); k++)
                {
                    if(dupa[k]== '/')
                    {
                        frac++;
                    }
                }
                if(frac != 0)
                {
                    desen(dupa, 0, x - 30 + egalitate2 * 15, y + 25 * nivel, 0);
                }
                else
                {
                    desen(dupa, 0, x - 15 + egalitate2 * 15, y + 15 * nivel, 0);
                }
            }
            if(m_formula(inainte, inainte.length(), m) == 0)
            {
                desen(inainte, 0, x - 15 + egalitate1 * 15, y - 25, 0);
            }
            else
            {
                int frac = 0;
                for(int k = 0; k < inainte.length(); k++)
                {
                    if(inainte[k] == '/')
                    {
                        frac++;
                        break;
                    }
                }
                if(frac != 0)
                {
                    desen(inainte, 0, x - 30 + egalitate1 * 15, y - 45, 0);
                }
                else
                {
                    desen(inainte, 0, x - 15 + egalitate1 * 15, y - 25, 0);
                }
            }
            j = j + l;
            if(a1 >= a2)
            {
                dif = dif + l - a1 - 1;
            }
            else
            {
                dif = dif + l - a2 - 1;
            }
            text = "";
        }
        if((e[0] == '(' && c[j+l1] == '^') || e[0] == '^' || (e[0] == '(' && (c[j+l1] == '+' || c[j+l1] == '-' || c[j+l1] == '*' || c.length() - (j+l1) == 0)))
        {
            l = l1 + l2;
            if(e[0] == '(')
            {
                if(text != "")
                {
                    x = poz1 + 15 * (j + 1 - dif);
                    repr(text, 0, x - 15 * (text.length()), y);
                    text = "";
                }
                else
                {
                    x = poz1 + 15 * (j + 1 - dif);
                }
                int p = 0;
                p = 1 + 6 * m_formula(inainte, inainte.length(), m);
                if(p > 10)
                {
                    p = 10;
                }
                settextstyle(5, 0, p);
                outtextxy(x, y - 2 * p, "(");
                x = poz1 + 15 * (j + a1 + 2 - dif);
                if(m_formula(inainte, inainte.length(), m) == 0 || m_formula(inainte, inainte.length(), m) == 1)
                {
                    outtextxy(x + (-1 * p), y - 2 * p, ")");
                }
                else
                {
                    outtextxy(x + (-3) * (l1-a1-p) + 10, y - 2 * p, ")");
                }
                if(p == 10)
                {
                    x = poz1 + 15 * (j + 2 - dif);
                }
                else
                {
                    x = poz1 + 15 * (j + 1 - dif);
                }
                settextstyle(5, 0, 1);
                desen(inainte, 0, x, y, 0);
                text = "";
                y = y - 12 * m_formula(inainte, inainte.length(), m);
                if(m_formula(inainte, inainte.length(), m) == 0 || m_formula(inainte, inainte.length(), m) == 1)
                {
                    dif = dif;
                }
                else
                {
                    dif = dif + l1 - a1 - 2 - m_formula(inainte, inainte.length(), m);
                }
            }
            if(e[0] == '^')
            {
                x = poz1 + 15 * (j + 1 - dif);
                repr(inainte, 0, x - 15 * (inainte.length()), y);
                text = "";
                dif = dif + l1 - a1;
            }
            if(c[j + l1] == '^' || e[0] == '^')
            {
                dif = dif + 1;
                if(e[0] == '^')
                {
                    x = poz1 + 15 *(j + 2 - dif);
                }
                if(c[j + l1] == '^')
                {
                    x = poz1 + 15 * (j+ l1 + 2 - dif);
                }
                if(m_formula(dupa, dupa.length(), m) == 0)
                {
                    desen(dupa, 0, x - 15, y - 10, 0);
                }
                if(m_formula(dupa, dupa.length(), m) != 0)
                {
                    int frac = 0;
                    for(int k = 0; k < dupa.length(); k++)
                    {
                        if(dupa[k] == '/')
                        {
                            frac++;
                            break;
                        }
                    }
                    if(frac != 0)
                    {
                        y = y - 30;
                    }
                    else
                    {
                        y = y - 10;
                    }
                    desen(dupa, 0, x - 15, y, 0);
                }
                text = "";
                j = j + l - 1;
                dif = dif + l2 - a2;
            }
            else
            {
                j = j + l - 1;
            }
        }
        if(c.length() - j == 1 && text != "")
        {
            x = poz1 + 15 * (j + 2 - dif);
            repr(text, 0, x - 15 * (text.length()), y);
            text = "";
        }
        j++;
    }
}

void inceput();
void citire_functie();

bool clickFunction(int leftCoordinate, int rightCoordinate, int topCoordinate, int bottomCoordinate) //Robu Victor
{
    const int DELAY = 50;
    int x, y;
    while (!ismouseclick(WM_LBUTTONDOWN)) delay(DELAY);

        getmouseclick(WM_LBUTTONDOWN, x, y);


        if(x > leftCoordinate && x < rightCoordinate && y > topCoordinate && y < bottomCoordinate) return true;
}

void ReadInput(char text[MAX_INPUT_LEN], int x, int y) //S�rbu Valentina
{
    outtextxy(510, 50, ":");
    settextstyle(5, 0, 1);
    int input_pos = 0;
    int the_end = 0;
    char e;
    while (!the_end)
    {
        outtextxy (x, y, text);
        e = getch();
        switch (e)
        {
        case 8: // backspace
            if (input_pos)
            {
                input_pos--;
                text[input_pos] = 0;
            }
            break;
        case 13: // enter
            the_end = 1;
            break;
        case 27: // Escape = Abort
            text[0] = 0;
            the_end = 1;
            break;
        default:
            if (input_pos < MAX_INPUT_LEN-1 && e >= ' ' && e <= '~')
            {
                text[input_pos] = e;
                input_pos++;
                text[input_pos] = 0;
            }
        }
    }
}

void desen1(string text) //S�rbu Valentina
{
    int window1;
    window1 = initwindow(800, 600, "Formula", 500, 100, false, false);
    readimagefile("note.jpg", 0, 0, 800, 600);
    setbkcolor(WHITE);
    setcolor(BLACK);
    int j = 0, x, y, dif = 0;
    x = 120;
    y = 250;
    settextstyle(5, 0, 1);
    desen(text, j, x, y, dif);
    readimagefile("1.jpg", 100, 500, 150, 550);
    outtextxy(170, 520, "Value : ");
    string input = unarMinus(text);
    input = unarPlus(input);
    int k = 0, postfixNumber = 0;
    vectorToken tokenized = tokenize(input, k);
    vectorToken postFixedTokenized = infixToPostfix(tokenized, k, postfixNumber);
    string val1 = expressionValue(postFixedTokenized,postfixNumber);
    char *tab2 = new char [val1.length()+1];
    strcpy(tab2, val1.c_str());
    outtextxy(230, 520, tab2);

    if(clickFunction(100, 150, 500, 550))
    {
        setcurrentwindow(0);
        setactivepage(0);
        setvisualpage(0);
        closegraph(window1);
    }
}

void desen2(string text) //Both
{
    int window2;
    window2 = initwindow(800,600, "Tree", 300, 100, false, false);
    string input = unarMinus(text);
    input = unarPlus(input);
    int k = 0, postfixNumber = 0;
    vectorToken tokenized = tokenize(input, k);
    vectorToken postFixedTokenized = infixToPostfix(tokenized, k, postfixNumber);
    int width = 800, height = 600;
    node * expression = constructTree(postFixedTokenized, postfixNumber);
    int latime = width / widthTree(expression);
    int inaltime = height / heightTree(expression);
    drawTree(expression, 1, 0, latime, inaltime);
    readimagefile("1.jpg", 50, 500, 100, 550);
    if(clickFunction(50, 100, 500, 550))
    {
        setcurrentwindow(0);
        setactivepage(0);
        setvisualpage(0);
        closegraph(window2);

    }
}

void citire_functie() //Both
{
    char text[MAX_INPUT_LEN] = "";
    string ceva, copyText, autogenerated;
    char * tab2, * autogen;
    settextstyle(5, 0, 4);
    setcolor(BLACK);
    setbkcolor(LIGHTGRAY);
    readimagefile("white.jpg", 0, 0, 800, 600);
    outtextxy(150, 50, "Enter the math formula ");
    readimagefile("1.jpg", 50, 500, 100, 550);
    outtextxy(650, 130, "Reset");
    outtextxy(550, 50, "Autogenerate");

    int buton, x, y;
    bool click = false;
    while(!click)
    {
        if(ismouseclick(WM_LBUTTONDOWN))
        {
            clearmouseclick(WM_LBUTTONDOWN);
            x = mousex();
            y = mousey();

            if((x >= 150 && x <= 500) && (y >= 30 && y <= 100))
            {
                click = true;
                outtextxy(510, 50, ":");
                ReadInput(text, 300 , 100);
            }
            if((x >= 550 && x <= 650) && (y >= 30 && y <= 100))
            {
                click = true;
                autogenerated = autoGenerateFormula();
                autogen = new char [autogenerated.length()+1];
                strcpy(autogen, autogenerated.c_str());
                strcpy(text, autogen);
                settextstyle(5, 0 ,1);
                outtextxy(200, 100, text);
            }
            if((x >= 50 && x <= 100) && (y >= 500 && y <= 550))
            {
                click = true;
                cleardevice();
                inceput();
            }
        }
    }
    settextstyle(5, 0, 4);
    if(validateStringAtCharLevel(text))
    {
        outtextxy(150, 130, "Valid");
        outtextxy(450, 520, "Draw me");
        outtextxy(250, 520, "Draw Tree");
    }
    else
    {
        outtextxy(150, 130, "Invalid");
        copyText = text;
        ceva = tryFixExpression(copyText);
        tab2 = new char [ceva.length()+1];
        strcpy(tab2, ceva.c_str());
        outtextxy(630, 175, "Autocorect");
        click = false;
        while(!click)
        {
            if(ismouseclick(WM_LBUTTONDOWN))
            {
                clearmouseclick(WM_LBUTTONDOWN);
                x = mousex();
                y = mousey();
                if((x >= 630 && x <= 700) && (y >= 175 && y <= 200))
                {
                    click = true;
                    if(validateStringAtCharLevel(ceva))
                    {
                        settextstyle(5, 0, 1);
                        outtextxy(150, 175, tab2);//SIRUL VALIDAT
                        settextstyle(5, 0, 4);
                        outtextxy(150, 220, "Is it what you meant?");
                        outtextxy(150, 260, "Yes");
                        outtextxy(400, 260, "No");
                        click = false;
                        while(!click)
                        {
                            if(ismouseclick(WM_LBUTTONDOWN))
                            {
                                clearmouseclick(WM_LBUTTONDOWN);
                                x = mousex();
                                y = mousey();
                                if((x >= 150 && x <= 200) && (y >= 260 && y <= 310))
                                {
                                    click = true;
                                    outtextxy(450, 520, "Draw me");
                                    outtextxy(250, 520, "Draw Tree");
                                    click = false;
                                    while(!click)
                                    {
                                        if(ismouseclick(WM_LBUTTONDOWN))
                                        {
                                            clearmouseclick(WM_LBUTTONDOWN);
                                            x = mousex();
                                            y = mousey();
                                            if((x >= 450 && x <= 600) && (y >= 520 && y <= 550))
                                            {
                                                //cleardevice();
                                                desen1(tab2);
                                            }
                                            if((x >= 250 && x <= 400) && (y >= 520 && y <= 550))
                                            {
                                                desen2(tab2);
                                                setactivepage(0);

                                            }
                                            if((x >= 650 && x <= 700) && (y >= 130 && y <= 180))
                                            {
                                                click = true;
                                                cleardevice();
                                                citire_functie();
                                            }
                                        }
                                    }
                                }
                                if((x >= 400 && x <= 450) && (y >= 260 && y <= 310))
                                {
                                    click = true;
                                    outtextxy(50, 305, "The Artificial Intelligence used in this program ");
                                    outtextxy(50, 345, "is not smart enough to fix your input");
                                }
                            }
                        }
                    }
                    else
                    {
                        outtextxy(50, 305, "The Artificial Intelligence used in this program ");
                        outtextxy(50, 345, "is not smart enough to fix your input");
                        //imagine cu meme despre AI;
                    }
                }
                if((x >= 650 && x <= 700) && (y >= 130 && y <= 180))
                {
                    click = true;
                    cleardevice();
                    citire_functie();
                }
            }
        }
    }
    click = false;
    while(!click)
    {
        if(ismouseclick(WM_LBUTTONDOWN))
        {
            clearmouseclick(WM_LBUTTONDOWN);
            x = mousex();
            y = mousey();
            if((x >= 450 && x <= 600) && (y >= 520 && y <= 550))
            {
                desen1(text);
            }
            if((x >= 250 && x <= 400) && (y >= 520 && y <= 550))
            {
                desen2(text);
                setactivepage(0);

            }
            if((x >= 650 && x <= 700) && (y >= 130 && y <= 180))
            {
                click = true;
                cleardevice();
                citire_functie();
            }
            if((x >= 50 && x <= 100) && (y >= 500 && y <= 550))
            {
                click = true;
                cleardevice();
                inceput();
            }
        }
    }
}

void details() //S�rbu Valentina
{
    int buton, x, y;
    readimagefile("math1.jpg", 0, 0, 800, 600);
    readimagefile("2.jpg", 90, 430, 140, 480);
    setcolor(BLACK);
    setbkcolor(WHITE);
    settextstyle(5, 0, 4);
    outtextxy(250, 150, "Proiect IP nr.23 ");
    settextstyle(5, 0, 3);
    outtextxy(100, 200, "Math - Editor de formule matematice");
    outtextxy(100, 225, "S�rbu Valentina /Robu Victor /  1A1");
    outtextxy(100, 275, "Functions you could draw:");
    settextstyle(5, 0, 2);
    outtextxy(100, 300, "1. Ordinary binary functions like +,-,*");
    outtextxy(100, 325, "2. Radicals");
    outtextxy(100, 350, "3. Fractions");
    outtextxy(100, 375, "4. Power of numbers");
    settextstyle(5, 0, 4);
    outtextxy(150, 420, "Caution : Functions` priority should ");
    outtextxy(150, 450, "be marked by paranthesis.");
    bool click = false;
    while(!click)
    {
        if(ismouseclick(WM_LBUTTONDOWN))
        {
            clearmouseclick(WM_LBUTTONDOWN);
            x = mousex();
            y = mousey();
            if((x >= 90 && x <= 140) && (y >= 430 && y <= 480))
                click = true;
            rectangle(90, 430, 140, 480);
            cleardevice();
            inceput();
        }
    }
}

void inceput() //S�rbu Valentina
{
    int buton, x, y;
    readimagefile("math.jpg", 0, 0, 800, 600);
    bool click = false;
    while(!click)
    {
        if(ismouseclick(WM_LBUTTONDOWN))
        {
            clearmouseclick(WM_LBUTTONDOWN);
            x = mousex();
            y = mousey();
            if((x >= 300 && x <= 500) && (y >= 300 && y <= 450))
            {
                click = true;
                cleardevice();
                details();
            }
            if((x >= 300 && x <= 500) && (y >= 200 && y <= 350))
            {
                click = true;
                cleardevice();
                citire_functie();
            }
        }
    }
}

int main()
{
    int window1;
    int height = 600, width = 800;
    window1 = initwindow(width, height, "Math", 100, 100, false, true);
    inceput();
    getch();
    closegraph();
    return 0;
}
